if ! ENV['AUTOPKGTEST_TMP']
  path = Dir['debian/*/usr/bin'].map do |d|
    File.absolute_path(d)
  end
  ENV['PATH'] = path.join(':') + ':' + ENV['PATH']
  puts "I: PATH = #{ENV['PATH']}"
end

REJECT = File.read('debian/ruby-tests.blacklist').split

task :default do
  tests = Dir['spec/**/*_spec.rb'].reject{ |spec| spec =~ /windows|win32/ } - REJECT
  tests = ENV['TESTS'].split if ENV['TESTS']
  ruby '-S', 'rspec', '-I.', *tests
end
